#include "pty.hh"
#include "window.hh"


int main(int argc, char *argv[]) 
{
	PTYInfo pty;
	pty.startPty();

	if (pty.master == -1) return 1;

	Window window(pty);
	std::cout << ", master: " << pty.master 
						<< ", slave: " << pty.slave << std::endl;

	window.init();

	return 0;
}
