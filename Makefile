CXX := g++
CXXFLAGS := -std=c++17 -g -Iwindow -Itty
LDFLAGS := -lutil -lsfml-graphics -lsfml-window -lsfml-system

SRC := main.cc \
			 window/window.cc \
			 tty/pty.cc
OBJS := main.o \
			 	window/window.o \
			 	tty/pty.o

terminal: $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^

clean:
	rm -f $(OBJS) terminal
