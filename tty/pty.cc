#include "pty.hh"

#include <fcntl.h>
#include <cstdio>
#include <errno.h>
#include <pty.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>


void PTYInfo::error()
{
  this->master = -1;
  this->slave = -1;
}

void PTYInfo::startPty()
{
  char name[255];
  auto e = openpty(&master, &slave, &*name, nullptr, nullptr);
  if (e < 0) {
    std::cerr << "Error: Couldn't open tty (" << strerror(errno) << ")\n";
    error();
    return;
  }

  if (!_fork()) {
    std::cerr << "Error: Couldn't fork tty (" << strerror(errno) << ")\n";
    error();
    return;
  }
}

bool PTYInfo::_fork()
{
  pid_t p;
  char *env[] = {"TERM=linux", NULL};

  p = fork();
  if (p == 0) {
    close(master);

    setsid();
    if (ioctl(slave, TIOCSCTTY, NULL) == -1) return false;

    dup2(slave, 0);
    dup2(slave, 1);
    dup2(slave, 2);
    close(slave);
    
    char *shell = getenv("SHELL");
    execle(shell, shell, (char *) NULL, env);
    return false;
  } else if (p > 0) {
    close(slave);
    return true;
  }

  return false;
}
