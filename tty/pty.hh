#ifndef __PTY_TERMINAL_HH__
#define __PTY_TERMINAL_HH__


struct PTYInfo
{
  int master, slave;

  void startPty();
  bool _fork();
  void error();
};

#endif
