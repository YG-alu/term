#ifndef __WINDOW_SFMLTERM_HH__
#define __WINDOW_SFMLTERM_HH__

#include <SFML/Graphics.hpp>

#include <iostream>

#include "pty.hh"

class Window
{
public:
  Window(PTYInfo);
  ~Window();

  void init();
  char _read();
  void _write(char);

  int master, slave;
  std::string name;

  int width, height;
  int fd;

  sf::Sprite cursor;
};

#endif
