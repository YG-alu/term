#include "window.hh"
#include "pty.hh"

#include <unistd.h>

Window::Window(PTYInfo info)
{
  this->master = info.master;
  this->slave = info.slave;
  this->name = ptsname(info.master);

  std::cout << "Name: " << name;

  width = 800;
  height = 600;
}

void Window::init()
{
  sf::RenderWindow window(sf::VideoMode(width, height), "Terminal");
  sf::Font font;
  if (!font.loadFromFile("/usr/share/fonts/TTF/FiraSans-Light.ttf")) {
    std::cerr << "Error: Couldn't load font\n";
    exit(1);
  }

  sf::Texture texture;
  if (!texture.loadFromFile("window/texture.png")) std::cerr << "Error: Couldn't cursor texture\n";
  

  cursor.setTexture(texture);
  cursor.setScale(sf::Vector2f(0.1, 0.05));
  cursor.setColor(sf::Color(255, 255, 255, 128));

  sf::Shader shader;
  if (!shader.loadFromFile("shader/shader.vert", "shader/shader.frag")) std::cerr << "Error: Couldn't load shader\n";

  float opacity = 1.0f;
  shader.setParameter("texture", sf::Shader::CurrentTexture);
  shader.setParameter("opacity", opacity);

	int fontSize = 15;
  sf::Text text;
  text.setFont(font);
  text.setCharacterSize(fontSize);
  text.setFillColor(sf::Color::White);
  
  sf::Clock clock;

	char fromTTY;
	sf::String fullString;
	
  while (window.isOpen()) {
    sf::Event event;

    fromTTY = _read();

    if (fromTTY != '\n') {
    	fullString += fromTTY;
    	text.setString(fullString);
    	text.move(sizeof(fromTTY)*fontSize, 0);
    } else {
      text.move(0, fontSize);
      text.setPosition(sf::Vector2f(0, text.getPosition().y));
      //cursor.move(0, fontSize);
      cursor.setPosition(sf::Vector2f(text.getPosition()  /*0, cursor.getPosition().y*/));
    	fullString = "";
    	text.setString(fullString);
    }

    if (cursor.getPosition().x == width) cursor.move(0, fontSize);
    else cursor.move(sizeof(fromTTY), 0);

    text.setString(_read());

    while (window.pollEvent(event)) {
      switch (event.type) {
      case sf::Event::Closed:
        window.close();
        break;
      case sf::Event::KeyPressed:
        switch (event.key.code) {
        case sf::Keyboard::Left:
          cursor.move(-(sizeof(fromTTY)*fontSize), 0);
          break;
        case sf::Keyboard::Right:
          cursor.move(sizeof(fromTTY)*fontSize, 0);
          break;
        case sf::Keyboard::Up: break; // TODO
        case sf::Keyboard::Down: break; // TODO
        } break;
      case sf::Event::TextEntered:
        _write(event.text.unicode);
        fullString += event.text.unicode;
        break;
      case sf::Event::Resized:
        width = event.size.width;
        height = event.size.height;
        break;
      case sf::Event::LostFocus:
        opacity = 0;
        clock.restart();  // Ignoring result here
  	    shader.setParameter("opacity", opacity);
        break;
      case sf::Event::GainedFocus:
        opacity = 1;
        clock.restart();
  	    shader.setParameter("opacity", opacity);
        break; 
      }
    }

    if (clock.getElapsedTime().asSeconds() > 0.001) {
	    opacity -= 0.1;
	    if (opacity < 0.0) opacity = 1.0;
	    clock.restart();
	    shader.setParameter("opacity", opacity);
    }


    window.clear(sf::Color::Black);
    window.draw(cursor, &shader);
    window.draw(text);
    window.display();
  }
}

char Window::_read()
{
  char _buffer;
  if (read(this->master, &_buffer, 1) <= 0) {
    std::cerr << "Error: Couldn't read from tty\n";
    exit(1);
  }
  return _buffer;
}

void Window::_write(char buffer)
{
  write(this->master, &buffer, 1);
}

Window::~Window()
{
  exit(0);
}
